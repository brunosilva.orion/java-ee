package br.com.fgp.arquitetura.software.java;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Queue;

/**
 * @author Filipe Bojikian Rissi
 * @since 1.0
 */
public class MessageProducer {

    @Inject
    private JMSContext ctx;

    @Resource(lookup = "java:global/queue/Persist")
    private Queue queue;

    public void sendMessage(String msg) {
        ctx.createProducer().send(queue, msg);
    }

}
